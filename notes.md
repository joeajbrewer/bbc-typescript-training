# Git 
cd ..
git clone https://bitbucket.org/ev45ive/jbi-bbc-typescript3.git jbi-bbc-typescript3
cd jbi-bbc-typescript3

cd examples/backend

npm i 
npm dev


## Installations
https://nodejs.org/en/
node -v
v14.17.0

npm -v
6.14.6

https://code.visualstudio.com/
code -v
1.56.2

git --version
git version 2.31.1.windows.1

docker -v
Docker version 20.10.6, build 370c289

chrome://version
Google Chrome	90.0.4430.212 (Official Build) (64-bit)


## TypeScript compiler
npm install --global typescript 
npm i -g typescript 

C:\Users\PC\AppData\Roaming\npm\tsc -> C:\Users\PC\AppData\Roaming\npm\node_modules\typescript\bin\tsc
C:\Users\PC\AppData\Roaming\npm\tsserver -> C:\Users\PC\AppData\Roaming\npm\node_modules\typescript\bin\tsserver
+ typescript@4.3.2


tsc examples/basics.ts
tsc --strict examples/basics.ts
tsc --watch examples/basics.ts

## TypeCheck JavaScript
https://code.visualstudio.com/docs/nodejs/working-with-javascript#_type-checking-javascript


## Playground + Strict mode
https://www.typescriptlang.org/play

## Initialize typescript project
tsc --init
tsc --strict --target es2015 --init
message TS6071: Successfully created a tsconfig.json file.

## NodeJS Typescript config
"target": "es6", 
"module":"CommonJS",
"lib": ["ES2020"],       
"moduleResolution": "node"

npm i --save-dev @types/node
<!-- build -->
tsc
<!-- run -->
node dist/index.js someArg A otherArg B
<!-- 
Hello backend
[ 'someArg', 'A', 'otherArg', 'B' ]
 -->

## Source maps
"sourceMap": true,  /* Generates corresponding '.map' file. */
"inlineSourceMap": true,  /* Emit a single file with source maps instead of having a separate file. */
   
node --enable-source-maps ./dist/index.js

Error: Ups..
    at Object.<anonymous> (.\examples\backend\dist\index.js:10:7)
        -> .\examples\backend\src\index.ts:11:7

## Debugging
 node --enable-source-maps --inspect dist/
 node --enable-source-maps --inspect-brk dist/

chrome://inspect/#devices

Vs code F5 - Run > Start Debugging

Debugger listening on ws://127.0.0.1:9229/a27a1b5c-63a4-4491-9ae5-b3aa6c848617
For help, see: https://nodejs.org/en/docs/inspector

## Productivity tools
tsc --watch
ts-node
nodemon
ts-node-dev

## Proxy, registry settings 
<!-- no httpS -->
npm config set registry http://registry.npmjs.org/
<!-- Ask your ADmin / Collegues ;-) -->
npm config set proxy http://username:password@cacheaddress.com.br:80

## Frontend 
npm init -y
tsc --strict --init

## Webpack
npx webpack init .
[webpack-cli] For using this command you need to install: '@webpack-cli/generators' package
[webpack-cli] Would you like to install '@webpack-cli/generators' package? (That will run 'npm install -D @webpack-cli/generators') (Y/n) y

npx webpack init .
? Which of the following JS solutions do you want to use? Typescript
? Do you want to use webpack-dev-server? Yes
? Do you want to simplify the creation of HTML files for your bundle? Yes
? Which of the following CSS solutions do you want to use? SASS
? Will you be using CSS styles along with SASS in your project? Yes
? Will you be using PostCSS in your project? No
? Do you want to extract CSS for every file? Yes
? Do you like to install prettier to format generated configuration? Yes
[webpack-cli] ℹ INFO  Initialising project...
 conflict package.json
? Overwrite package.json? overwrite
    force package.json
 conflict src\index.ts
? Overwrite src\index.ts? overwrite
    force src\index.ts
   create README.md
 conflict index.html
? Overwrite index.html? overwrite
    force index.html
   create webpack.config.js
 conflict tsconfig.json
? Overwrite tsconfig.json? overwrite
    force tsconfig.json

https://webpack.js.org/api/cli/


$ webpack configtest
[webpack-cli] Validate 'C:\Projects\szkolenia\bbc-typescript3\examples\frontend\webpack.config.js'.
[webpack-cli] There are no validation errors in the given webpack configuration.

## Generating JS + Ambient Types (+ documentation)
tsc --declaration examples/functions.ts


## QuickType type generator
https://quicktype.io/typescript
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype


## Pattern matching
https://github.com/gvergnaud/ts-pattern

## Classes, inheritance
https://www.typescriptlang.org/docs/handbook/mixins.html