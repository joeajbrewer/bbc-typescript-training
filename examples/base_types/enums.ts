
type MAGIC_NUMBERS = 1234 | 2048 | 3006;
// let x: MAGIC_NUMBERS = 4 // error

enum HTTP_CODES {
    OK = 200,
    BAD_REQUEST = 400,
    UNAUTHORIZED, // 401
    FORBIDDEN = 403, // 403
    I_AM_A_TEAPOT, // 418
}

enum InvoiceStatus {
    SUBMITTED = 'SUBMITTED',
    APPROVED = 'APPROVED',
    PAID = 'PAID',
    LEASED = 42,
    UNLEASED // 43
}

debugger
const result = InvoiceStatus.APPROVED
console.log(InvoiceStatus[result]);


function getStatusLabel(status: InvoiceStatus) {
    switch (status) {
        case InvoiceStatus.SUBMITTED:
            return "invoice was submitted";
        case InvoiceStatus.APPROVED:
            return `it's approved`;
        case InvoiceStatus.PAID:
            return "invoice paid";
    }
}

enum FileAccess {
    None = 1 << 0,
    READ = 1 << 1,
    WRITE = 1 << 2,
    Execute = 1 << 3,
    ReadWrite = FileAccess.READ | FileAccess.WRITE
}

enum UserRole {
    USER = "user",
    ADMIN = "admin",
    MODERATOR = "moderator",
};

function getPermissionsFor(role: UserRole): number | undefined {
    switch (role) {
        case UserRole.ADMIN:
            return 123;
        case UserRole.USER:
            return undefined;
        case UserRole.MODERATOR:
            return undefined;
        default:
            const _never: never = role; // exhaustiveness check
            throw new Error('No role' + _never)
    }
}