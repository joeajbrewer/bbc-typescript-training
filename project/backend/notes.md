## NodeJS + Typescript
npm init -y
tsc --strict --lib es2020 --target es2015 --outDir ./dist --inlineSourceMap --init

git init .
echo "/node_modules" >> .gitignore
echo "/dist" >> .gitignore

npm i express
npm i -D ts-node ts-node-dev typescript @types/node @types/express 

mkdir src
echo "console.log('test')" > ./src/index.ts

tsc

## Testing
https://kulshekhar.github.io/ts-jest/docs/getting-started/installation

npm install --save-dev jest typescript ts-jest @types/jest
npx ts-jest config:init
