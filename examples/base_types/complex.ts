export { }

/* Object */
function create(o: object | null): void { }
// OK
create({ prop: 0 });
create(new Date());
create(new Array());
create(null); // typeof null === 'object'
// create(42);  // Error

const myArray = Object.create(Array.prototype)
const myError = Object.create(Error.prototype)

/* Arrays */

// Way 1: Type[]
let list1: number[] = [1, 2, 3];

// Way 2: Array<Type>
// The second way uses a generic array type, Array<elemType>:
let list2: Array<number> = [1, 2, 3];

let list3 = [1, '2', true];
list3.push(true, 123)
list3.push('123')

/* Tuples */

// This is NOT and Array! This is a tuple!
let notAList: [number] = [1]
// let notAList: [number] = [1,2] // Error

let iAmATuple1: [number, string] = [1123, 'user@email.com']
let iAmATuple2: [userId: number, userEmail: string] = [1123, 'user@email.com']
const [userId, userEmail] = iAmATuple2

function displayUser(user: [userId: number, userEmail: string]) { }
function displayUser2(...param: [userId: number, userEmail: string]) { }
// displayUser2(userId: number, userEmail: string): void

/* Any vs Type Assertion */

let name1: 'Albert' = 'Albert'
let name2: string = 'Alice'
name2 = name1
// name1 = name2  // error 
name1 = name2 as 'Albert'

let x = {};
let y: string[] = [];

x = y
y = x as Array<string>
// y = x // error


let someValue: unknown = "this is a string";

// let strLength: number = someValue.length // error
// let strLength2: number = someValue // error

let strLength2: number = (someValue as string).length
let strLength: number = ((typeof someValue === 'string') ? someValue : '').length


/* Complex types */
// const item: Item = { "name": "item", "value": 42 }
const item = { "name": "item", "value": 42, "extra": "stuff" }

// type Item = {
interface Item {
    name: string;
    /**
     * Item price value in canadian dollars
     */
    value: number;
}

// function printItem(item: { name: string; value: number; }) {
function printItem(item: Item) {
    return item.name + ' : ' + item.value
}
printItem({ name: '123', value: 123 })
printItem(item)


/* Structural Typing ( duck typing ) */

interface Vector { x: number; y: number, length: number }
interface Point { x: number; y: number }

let v: Vector = { x: 123, y: 123, length: 1230 }
let p: Point = { x: 123, y: 123 }

p = v
// v = p // Error
// v = p as Vector // Unsafe - length == undefined

/* Type vs Interface */
type myLabel = 'LABEL_CONSTANT'
const label: myLabel = 'LABEL_CONSTANT'

type MaybeStringOrNot = string | number | undefined | boolean
type intersection = Vector & Point

/* declaration */
interface NewThing { }

/* alias */
type athing = NewThing;
const thing: athing = {} // thing: NewThing


/* Declaration Merging */
// base.ts
interface BaseLibraryInterface { base: string }

// plugin.ts
interface BaseLibraryInterface { extras: string }

const request: BaseLibraryInterface = {
    base: '', extras: ''
}



/* Optional properities */
interface Entity {
    id: number | string;
    name: string;
}

interface Track extends Entity {
    type: 'Track', // discriminator
    duration: number
}
interface Playlist extends Entity {
    type: 'Playlist',
    // tracks?: Array<{ id: number, name: string }>
    tracks?: Array<Track>
}
// type PlaylistId = Playlist['id']

// interface PlaylistSnapshot {
//     id: Playlist['id']
// }

const playlist: Playlist = {
    id: '123', name: '123', type: 'Playlist', tracks: [
        { id: '123', name: '123', type: 'Track', duration: 123 },
        { id: '234', name: '234', type: 'Track', duration: 123 },
    ]
};

(playlist.id as string).toUpperCase();


if (typeof playlist.id === 'string') {
    playlist.id.toLocaleLowerCase()
} else {
    playlist.id.toPrecision(0).toLocaleLowerCase()
}

if (playlist.tracks) {
    const len1 = playlist.tracks.length
}
const len2 = playlist.tracks ? playlist.tracks.length : 0 // number
const len3 = playlist.tracks?.length //  number | undefined
const len4 = playlist.tracks?.length || 0 //  number 
const len5 = playlist.tracks?.length ?? 0 // number  // es2020
const len6 = playlist.tracks && playlist.tracks.length || 0 //  number 
const len7 = (playlist.tracks || []).length //  number 

const lenX = playlist.tracks!.length //  number, but really?
const lenX2 = (playlist.tracks as Track[]).length //  number, but really?

/* Discriminated union */
type SearchResult = Playlist | Track
// type SearchResult = Playlist | Track | { type: 'Pancakes' }
// const searchresult = {} as SearchResult;

function renderResult(result: SearchResult) {
    switch (result.type) {
        case 'Playlist': {
            return `Playlist ${result.name}`
        }
        case 'Track': {
            return `Track ${result.name} ${result.duration}`
        }
        default: {
            // const _: never = result
            exhaustivnessCheck(result)
        }
    }
}

function exhaustivnessCheck(_never: never): never {
    throw new Error('unsuported type ' + (_never as SearchResult).type)
    // throw new Error('Unexpected case ' + _never)
}

interface ImmutablePoint {
    readonly x: number;
    readonly y: number;
}
// After the assignment, x and y can’t be changed.
let p1: ImmutablePoint = { x: 10, y: 20 };
// p1.x = 5; // error!
// Error: Cannot assign to 'x' because it is a read-only property.
// (p1 as any).x = 5; // "anything" goes!
const p2 = Object.freeze({ x: 10, y: 20 } as Point) //  Readonly<Point> 
// p2.x = 123 // Error - property readonly
// Error: Cannot assign to 'x' because it is a read-only property.

/* Indexed types */

interface MyDictionary {
    [term: string]: string;
    // [term: string]: string | number;
    // [term: number]: number; // incompatibile
    // [term: number]: string ; // ok
    // [term: number]: string | number;
    // ok if  [term: string]: string | number;
}

type MyDictionary2 = {
    [term in string | number]: string;
}

let dict: MyDictionary = { 'termA': 'description..' }
dict['termB'] = 'more  descriptions...'
dict['termC'] = 'more descriptions...'
dict[123] = 'more descriptions...'
// dict[123] = 123 // error

/* Extracting types from types */


interface PlaylistSnapshot {
    id: Playlist['id']
}

interface PlaylistsCache {
    // An index signature parameter type cannot be a union type. Consider using a mapped object type instead.ts(1337)
    //  [playlist_id: Playlist['id']]: Playlist
    [playlist_id: string]: Playlist
}

type PlaylistId = Playlist['id']
// type PlaylistId = 'name' | 'id' | '123' | '234'

type PlaylistsCache2 = {
    // [playlist_id in Playlist['id']]: Playlist
    [playlist_id in PlaylistId]: Playlist
}
const cache: PlaylistsCache2 = {}
cache['123'] = {} as Playlist
cache['123' + 2] = {} as Playlist
// cache[132] = {} as Playlist
cache['234'] = {} as Playlist



type RequestCredentials =
    | 'omit'
    | 'same-origin'
    | 'include'

type RequestOptions = {
    credentials: RequestCredentials
}

const requestopt: RequestOptions = {
    credentials: 'omit',
    // credentials: 'pancakes' // Error
}

const div = document.createElement('div') //HTMLDivElement
const a = document.createElement('a') //HTMLAnchorElement

// type IDprefix = `credentials_${Lowercase<RequestCredentials>}`
type IDprefix = `credentials_${Uppercase<RequestCredentials>}`
const cred: IDprefix = 'credentials_SAME-ORIGIN'

type MarkerTime =`${number| ''}${number}:${number}${number}`

// let a1: MarkerTime = "0-00" // error
let b2: MarkerTime = "0:00" // ok
let c3: MarkerTime = "09:00" // ok


type Headers = 'Accept' | 'Authorization'
type ExtraHeaders = 'X-Proxy'
// type MergedHeaders = Headers | 'X-Proxy'
type MergedHeaders = Headers | ExtraHeaders

type RequestConfig = {
    // [optionKey in MergedHeaders]: string
    [optionKey in MergedHeaders]?: string
}
// type RequestConfig = {
//     Accept?: string | undefined;
//     Authorization?: string | undefined;
//     "X-Proxy"?: string | undefined;
// }
const requestConfig: RequestConfig = {
    Accept: '',
    Authorization: '',
    // "X-Proxy": ''
}

type RequestOptionKey = keyof RequestConfig
const c: RequestOptionKey = 'X-Proxy'

const ConfigArray: Array<RequestOptionKey> = ['Accept', "Authorization", "X-Proxy"]
const ConfigTuple: [RequestOptionKey, string] = ["Authorization", "supertopsecrettoken"]


type TagElementMap = {
    a: HTMLAnchorElement,
    div: HTMLDivElement,
}
type Tags = keyof TagElementMap

const key: Tags = 'div'

// typeof '' === 'string'

type keyTypeLiteral = typeof key

// // 'key' refers to a value, but is being used as a type here. Did you mean 'typeof key'?ts(2749)
// type SelectedElementType = TagElementMap[key]

type SelectedElementType = TagElementMap[keyTypeLiteral] // HTMLDivElement