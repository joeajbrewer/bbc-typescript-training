import { Express } from 'express';
import { booksRoutes } from './routes/books';

export function configureApp(app: Express) {

  app.get('/', (req, res) => {
    req.session.views = req.session.views ?? 0
    req.session.views++
    req.session.save()

    // req.secret === '123'

    res.send({
      message: 'Hello world!',
      views: req.session.views,
    });
  });

  app.use('/books', booksRoutes)
}



