"use strict";
exports.__esModule = true;
// type IAmString = string
// function add(x: IAmString, y: string) {
function add(x, y) {
    // if (x <= 0) { throw new Error('Error') }
    // ..
    return x + y;
}
// let nothing: string;
// nothing.toLocaleLowerCase()
var maybeNothing = '';
maybeNothing.toLocaleLowerCase();
// const a = 1
// const b = 2
// let result = add(a, b)
// let result = add('123', '123') // Error!
// let result = add(undefined, null)
// let result = add() // Error!
// let result = add('123', 123) // Error!
var result = add(123, 123);
// console.log(' Promo Price ' + result.toUpperCase())
console.log(' Price ' + result.toFixed(2));
