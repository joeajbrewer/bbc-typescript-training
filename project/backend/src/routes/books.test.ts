import request from 'supertest'
import { app } from '../app'
import { findBooks } from '../services/books'
import { FindBooksCriteria, FindBooksResults } from "../model/Book"
import { mocked } from 'ts-jest/utils'

jest.mock('../services/books')

describe('Books API', () => {

  // let mock: jest.MockedFunction<typeof findBooks>

  // beforeEach(() => {

  //   // type mock = jest.Mock<Promise<FindBooksResults>, [FindBooksCriteria]>
  //   // type Mock<R, P extends any[]> = jest.Mock<R, P>
  //   // type AutoMock<F extends (...args: any) => any> = jest.Mock<ReturnType<F>, Parameters<F>>
  //   // (findBooks as AutoMock<typeof findBooks>).mockResolvedValue({

  //   mock = mocked(findBooks).mockResolvedValue({
  //     items: [{ id: '123' }]
  //   })

  // })

  const setup = () => {
    const findBooksMock = mocked(findBooks).mockResolvedValue({
      items: []
    })

    return { findBooksMock }
  }

  test('Should serve list of books', async () => {
    const { findBooksMock } = setup()

    findBooksMock.mockResolvedValue({ items: [{ id: '123' }] })

    await request(app)
      .get('/books')
      .expect(200)
      .expect({
        items: [{ id: '123' }]
      })
    // .end((error, res) => {
    //   expect(res.body).toEqual([])
    // })

  })
})
