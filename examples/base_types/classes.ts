
// type Person = {}

// interface Person {
//     x: string
//     getX(): string
// }

// ...later...

// interface Person {
//     calc(): string
// }
// Person.prototype.calc = function () { return '' }
// const p = new Person()
// p.x = '123'
// p.getX()

interface IHaveId { id: string }
interface IHaveName { name: string, sayHello(): string }

class Person implements IHaveId, IHaveName {
    readonly type = 'Person' as const
    private _hash?: string = btoa(this.id + this.name)

    // id: string;
    // name: string;

    constructor(readonly id: string, public name: string) {
        // this.id = id
        // this.name = name
        this._hash = '000'
    }

    sayHello(): string {
        return `Hello, my name is ${this.name}`
    }
}

const p = new Person('123', 'Alice')


class Employee extends Person {

    constructor(id: string, name: string, public company: string) {
        super(id, name) // parent constructor call is required!
    }

    sayHello() {
        return super.sayHello() + ' I work at ' + this.company
    }

    doWork() {
        return 'I love working at ' + this.company
    }
}


const e = new Employee('123', 'Tom', 'ACME Corp.')



class Singleton {
    private static instance: Singleton
    private active = false

    private constructor() { }

    static activate(){
        this.activate()
    }

    static getInstance() {
        if (!Singleton.instance) {
            Singleton.instance = new Singleton()
        }

        return Singleton.instance
    }
}
// new Singleton() // Error - private constructor
Singleton.getInstance()
Singleton.activate()