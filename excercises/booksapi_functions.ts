import { BookData, GetBooksResponse, VolumeInfo } from "./booksapi"


const booksdata: GetBooksResponse = ({
    kind: 'books#volumes',
    totalItems: 3, items: [
        {
            id: '123', kind: 'books#volume', volumeInfo: {
                title: 'Typescript 123',
                authors: ['Author ABC'],
                pageCount: 123,
                categories: ['programming', 'js']
            }
        }, {
            id: '234', kind: 'books#volume', volumeInfo: {
                title: 'JavaScript 234',
                authors: ['Author ABC'],
                pageCount: 234,
                categories: ['programming', 'js']
            }
        }, {
            id: '345', kind: 'books#volume', volumeInfo: {
                title: 'React 345',
                authors: ['Author XYZ'],
                pageCount: 345,
                categories: ['programming', 'react']
            }
        }
    ]
});

// interface Criteria {
//     title?: string
//     author?: string
//     category?: string
// }

// // Criteria:
interface ByTitle { /* type: 'title', */ title: string }
interface ByAuthor { /* type: 'author', */ author: string }
interface ByCategory { /* type: 'category', */ category: string }

type Criteria = ByTitle | ByAuthor | ByCategory //| { byPageCount: number };


function findBooks(criteria: ByTitle): BookData[]
function findBooks(criteria: ByAuthor): BookData[]
function findBooks(criteria: ByCategory): BookData[]
function findBooks(criteria: Criteria): BookData[] {
    if ('author' in criteria) {
        return booksdata.items.filter(book =>
            book.volumeInfo.authors.find(a => a.toLocaleLowerCase().includes(criteria.author.toLocaleLowerCase()))
        )
    }

    if ('title' in criteria) {
        return booksdata.items.filter(book =>
            book.volumeInfo.title.toLocaleLowerCase().includes(criteria.title.toLocaleLowerCase()))
    }

    if ('category' in criteria) {
        return booksdata.items.filter(book =>
            book.volumeInfo.categories.includes(criteria.category)
        )
    }
    exhaustiveness(criteria)
    // const _never = criteria
    // throw new Error('Unexpected case ' + _never)
    // throw new NeverError(_never:never)
}

// Cannot be arrow function!!! // Why? 

// const _never: never = criteria
function exhaustiveness(_never: never): never {
    throw new Error('Unexpected case ' + _never)
}

// const findAllBooksByTitle = (title: VolumeInfo['title']) => {
//     return booksdata.items.filter(book =>
//         book.volumeInfo.title.toLocaleLowerCase().includes(title.toLocaleLowerCase())
//     )
// }
// const findAllBooksByAuthor = (author: string): BookData[] => {
//     return booksdata.items.filter(book =>
//         book.volumeInfo.authors.find(a => a.toLocaleLowerCase().includes(author.toLocaleLowerCase()))
//     )
// }
// const findAllBooksByCategory = (category: string) => {
//     return booksdata.items.filter(book =>
//         book.volumeInfo.categories.includes(category)
//     )
// }

const getBookById = (id: string): BookData => {
    const result = booksdata.items.find(book => book.id === id)
    if (!result) { throw new Error(`Book ${id} does not exist!`) }
    return result
}
interface CreateBookDataPayload {
    title: string;
    subtitle?: string;
    authors?: string[];
    publisher?: string;
    publishedDate?: Date;
    description?: string;
    pageCount?: number;
    categories?: string[];
}
interface UpdateBookDataPayload extends CreateBookDataPayload {
    id: string
}

const createBook = (payload: CreateBookDataPayload) => {
    if (!payload.authors || payload.authors.length === 0) {
        throw new Error('At least one author is required')
    }

    const newBook: BookData = {
        id: generateId(),
        kind: 'books#volume',
        volumeInfo: {
            categories: [],
            authors: payload.authors,
            pageCount: 0,
            description: '',
            ...payload,
        }
    }
    booksdata.items.push(newBook)
    booksdata.totalItems = booksdata.items.length
    return newBook
}
const updateBook = (payload: UpdateBookDataPayload) => {
    if (!payload.authors || payload.authors.length === 0) {
        throw new Error('At least one author is required')
    }

    const newBook: BookData = {
        id: generateId(),
        kind: 'books#volume',
        volumeInfo: {
            categories: [],
            authors: payload.authors,
            pageCount: 0,
            description: '',
            ...payload,
        }
    }
    const oldBook = getBookById(payload.id)
    booksdata.items[booksdata.items.indexOf(oldBook)] = newBook;
    return newBook
}
const deleteBook = (id: string) => {
    const oldBook = getBookById(id)
    delete booksdata.items[booksdata.items.indexOf(oldBook)]
    booksdata.totalItems = booksdata.items.length
    return oldBook
}

/* Book name - Author name (123 pages) */
const getBookInfo = ({
    volumeInfo: {
        title,
        authors = ['No Author'],
        categories = ['No Category'],
        pageCount: pageNum = 0
    }
}: BookData) => `${title} - ${authors[0]} (${pageNum} pages) ${categories[0]}`



// Conversion of type 'void' to type 'any[]' may be a mistake because neither type sufficiently overlaps with the other.
//  If this was intentional, convert the expression to 'unknown' first.ts(2352)
// const result = findAllBooksByTitle('react') as unknown as any[]
// const result = findAllBooksByAuthor('XYZ') as unknown as any[]

const result = findBooks({ author: 'XYZ' })

// result.map(getBookInfo).forEach(console.log)

function generateId(): string {
    return `${Date.now()}-${Math.ceil(Math.random() * 10000)}`;
}


enum SortKey {
    title = 'title',
    pageCount = 'pageCount',
    // Computed values are not permitted in an enum with string valued members
    // DEFAULT = SortKey.title
}
enum SortDir {
    ASC = 1, DESC = -1, DEFAULT = SortDir.ASC
}

// type BookSorter = {
//     dir: SortDir;
//     key: SortKey;
//     getComparator(): (a: BookData, b: BookData) => number;
// }
// type BookSorter = typeof bookSorter

const bookSorter = {
    dir: SortDir.DEFAULT,
    key: SortKey.title,
    getComparator() {
        // 'this' implicitly has type 'any' because it does not have a type annotation.ts(2683)
        // An outer value of 'this' is shadowed by this container.
        return (a: BookData, b: BookData) => {
            const aStr = String(a.volumeInfo[this.key]);
            const bStr = String(b.volumeInfo[this.key]);
            return (this.dir == SortDir.ASC) ?
                aStr.localeCompare(bStr, 'en', { numeric: true })
                : bStr.localeCompare(aStr, 'en', { numeric: true })
        }
        // return function (this: BookSorter, a: BookData, b: BookData) {
        // ...
        // }.bind(this)
    }
}

bookSorter.dir = SortDir.ASC
bookSorter.key = SortKey.title
// booksdata.items.sort(bookSorter.getComparator()).map(getBookInfo).forEach(console.log)
