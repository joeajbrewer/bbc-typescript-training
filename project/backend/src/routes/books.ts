import { Router } from 'express'
import { createBook, findBooks } from '../services/books'
import { Book, CreateBookDTO, FindBooksCriteria, FindBooksResults, isValidCreateBookDTO } from "../model/Book"

export const booksRoutes = Router()


booksRoutes.get<{}, FindBooksResults, null, FindBooksCriteria>('/', async (req, res) => {
  const criteria = req.query

  res.json(await findBooks(criteria))
})


booksRoutes.post<{}, Book, {}, {}>('/', async (req, res) => {
  // req.body  // any
  if (!isValidCreateBookDTO(req.body)) {
    throw new Error('invalid dto')
  }
  // req.body.title // string

  const result = await createBook(req.body)

  res.json(result)

})