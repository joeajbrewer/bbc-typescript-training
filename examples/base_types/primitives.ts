export { }

// let isThisBookOkay: boolean = true;
let isThisBookOkay = true;

isThisBookOkay = true
isThisBookOkay = false

/* Numbers  */
const num1 = 123
let num2 = 123

// number
const num3 = Number(123)
typeof num3 == 'number'
const num4 = Number('123')
typeof num3 == 'object'
const num5 = new Number('123')
// Number {123}__proto__: Number constructor: ƒ Number()

const result: number = 123 + 0xbeef + 0b1111 + 0o700;
// 49465

// const num6: number = new Number('123') // Error!

/* Strings */
String('test')
new String('test')

const firstName = "Mateusz";
// firstName === 'Mateusz1' // Error!
let lastName = "Kulesza";
const hello: string = `Hello,
${firstName} ${lastName}!`;
// Hello,\nMateusz Kulesza!

function getInfo(person: string) { }
getInfo(firstName)

/* Any */
function getValue(key: string): any { };


// OK, return value of 'getValue' is not checked
const str1: any = window.document
const str2: Document = getValue("myString");
const str3 = getValue("myString");
str3.this.doesnt.exit.and.its.not.typechecked

// Any propagates down to members:
let looselyTyped: any = {};
let d = looselyTyped.a.b.c.d.getMeAmillionDollars()
// ^ = let d: any

/* Void */

function warnUser(): void {
    console.log("This is my warning message");
}
// const nothing: undefined = warnUser()  // Error
const nothing: void = warnUser()
const nothing2 = warnUser()  // type == 'void'

/* Undefined and null */
const iamoptional: string | undefined = undefined;

// function setConfig(configval?: string) {
function setConfig(configval: string | undefined = undefined) {
    if ('undefined' === typeof configval) {
        configval == 'DEfault value'
    } else {
        configval.toLocaleLowerCase()
    }
}
setConfig()
setConfig('123')

typeof undefined
"undefined"
typeof null
"object"

/* Never */

let maybe = 'some unknown data from server' as string | number | undefined /* | boolean */
// let maybe: string | number = 'some unknown data from server' as string | number

// Type narrowing:
if (undefined === maybe) { //  string | number | undefined
    maybe = 'default value'
} else if (typeof maybe === 'string') { // string | number
    maybe.toLocaleLowerCase()
} else if (typeof maybe === 'number') { //  number
    maybe.toExponential()
} else {
    exhaustivnessCheck(maybe)
    // const _IamImpossibleChoice: never = maybe
    // maybe // never
}

function exhaustivnessCheck(_never: never): never {
    throw new Error('Unexpected case ' + _never)
}

const obj = {
    arry: [] // never[]
}
// obj.arry.push(123) // error

const arr = []
arr.push(123)
arr.push('123')

// Function returning never MUST NOT have a reachable end point
function error(message: string): never {
    // if(true){ return true } // not assignable to type 'never'.

    throw new Error(message);
}

// Inferred return type is never
function fail() {
    if (true) {
        error("Something failed")
        // console.log('test') // Unreachable code detected.
    };
} // :never

// Function returning never must not have a reachable end point
function infiniteLoop(): never {
    while (true) {
        // return '' // not assignable to never
    }
}

/* Literal types */

// let anyText1: string = 'Hello!'
const anyTextconst = 'Hello!'

let anyText1 = 'Hello!'
anyText1 = 'Goodbye!' // OK

let helloText: 'Hello' = 'Hello'
// helloText = 'Goodbye!' // Error!

// alternatively:
let helloText2 = 'Hello' as const
// helloText = 'Goodbye!' // Error!

let nestedObj = {
    SOME_CONST: 'SOME_CONST' as const
}

type configObj = typeof nestedObj
//  SOME_CONST: 'SOME_CONST';

function getConfig() {
    return [123, 'name'] as const
}
// const config1 = getConfig() // (string | number)[]
const config1 = getConfig() //  readonly [123, "name"]
// config.push(123)

function getConfig2(): [id: number, name: string] {
    return [123, 'name']
}

const config2 = getConfig2() //  [id: number, name: string]
config2[1] = 'changed name'