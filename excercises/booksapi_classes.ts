import { BookData, GetBooksResponse } from "./booksapi";


const booksdata: GetBooksResponse = ({
    kind: 'books#volumes',
    totalItems: 3, items: [
        {
            id: '123', kind: 'books#volume', volumeInfo: {
                title: 'Typescript 123',
                authors: ['Author ABC'],
                pageCount: 123,
                categories: ['programming', 'js']
            }
        }, {
            id: '234', kind: 'books#volume', volumeInfo: {
                title: 'JavaScript 234',
                authors: ['Author ABC'],
                pageCount: 234,
                categories: ['programming', 'js']
            }
        }, {
            id: '345', kind: 'books#volume', volumeInfo: {
                title: 'React 345',
                authors: ['Author XYZ'],
                pageCount: 345,
                categories: ['programming', 'react']
            }
        }
    ]
});

interface IBookConstructor {
    fromJSON(data: BookData): Book
}

interface IBookData {
    id: string
    title: string;
    subtitle?: string;
    authors: string[];
    publisher?: string;
    publishedDate?: Date;
    description?: string;
    pageCount: number;
    categories: string[];
}
interface ISerializeBook {
    toJSON(): BookData
}

interface IBookDTO {
    title: string;
    subtitle?: string | undefined;
    authors: string[];
    publisher?: string | undefined;
    publishedDate?: Date | undefined;
    description?: string | undefined;
    pageCount: number;
    categories: string[];
}
interface IBookCreateDTO extends IBookDTO { }
interface IBookUpdateDTO extends IBookDTO {
    id: string;
}
type ICriteria = { title: string } // TODO...

interface IDoReadBookRepository {
    find(criteria: ICriteria): Book[]
    getById(id: IBookData['id']): Book
}
interface IDoWriteBookRepository {
    create(data: IBookCreateDTO): Book
    update(data: IBookUpdateDTO): Book
    delete(id: IBookData['id']): Book
}


type BookCreateDTO = {
    id: string;
    title?: string;
    pageCount?: number;
    authors?: never[];
    categories?: never[];
};

class Book implements IBookData {
    id: string
    private _title: string;
    public get title(): string {
        return this._title;
    }
    public set title(value: string) {
        if (!this.title) { throw new Error('Title is required') }
        this._title = value;
    }
    pageCount: number;

    private _authors = new Set<string>();

    public get authors(): string[] {
        return Array.from(this._authors.values());
    }

    public set authors(value: string[]) {
        this._authors = new Set(value)
    }

    addAuthor(author: string) {
        this._authors.add(author)
    }

    categories: string[];

    subtitle?: string;
    publisher?: string;
    publishedDate?: Date;
    description?: string;


    private constructor({
        id,
        title = '',
        pageCount = 0,
        authors = [],
        categories = [],
    }: BookCreateDTO) {
        this.id = id
        this._title = title
        this.authors = authors
        this.pageCount = pageCount
        this.categories = categories
    }

    static create(payload: any): Book {
        const book = new Book(payload)
        return book
    }

    update({
        title = '',
        pageCount = 0,
        authors = [],
        categories = [],
    }: IBookUpdateDTO) {
        this.title = title
        this.pageCount = pageCount
        this.authors = authors
        this.categories = categories
        return this;
    }

    /* User Interface  */
    static fromJSON(payload: IBookCreateDTO) { }

    toJSON(): BookData {
        return {
            id: this.id,
            kind: 'books#volume',
            volumeInfo: {
                title: this.title,
                pageCount: this.pageCount,
                authors: this.authors,
                categories: this.categories,
            }
        }
    }
}

// https://en.wikipedia.org/wiki/SOLID

// class BookSQLRepository implements IDoWriteBookRepository {
// class BookMongoDbRepository implements IDoReadBookRepository {

class BookInMemoryRepository implements IDoReadBookRepository, IDoWriteBookRepository {

    constructor(private _books: BookData[] = booksdata.items) { }

    find(criteria: ICriteria): Book[] {
        return this._books.filter(book => book.volumeInfo.title.includes(criteria.title)).map(
            rawData => Book.create(rawData)
        )
    }

    getById(id: string): Book {
        const rawBook = this._books.find(book => book.id == id)
        if (!rawBook) { throw new Error(`Book ${id} does not exist`) }
        return Book.create({
            ...rawBook
        })

    }

    save(book: Book) { }

    create(data: IBookCreateDTO): Book {
        const newBook = Book.create(data)
        this._books.push(newBook.toJSON())
        return newBook
    }

    update(data: IBookUpdateDTO): Book {
        const oldBook = this.getById(data.id)
        const newBook = oldBook.update(data)
        const index = this._books.findIndex(b => b.id == data.id)
        this._books[index] = newBook.toJSON()
        return newBook
    }

    delete(id: string): Book {
        throw new Error("Method not implemented.");
    }
}