```js
class Person{
    constructor(name){
        this.name = name
    }

    sayHello(){
        return 'My name is ' + this.name
    }
}

class Employee extends Person{
    constructor(name,salary){
        super(name)
        this.salary = salary
    }

    sayHello(){
        return super.sayHello() + ' and I am employee!'
    }

    work(){
        return ' I need my ' + this.salary
    }
}
tom = new Employee('Tom',1600)

```
Employee
    name: "Tom"
    salary: 1600
    __proto__: Person
        constructor: class Employee
        sayHello: ƒ sayHello()
        work: ƒ work()
        __proto__: 
            constructor: class Person
            sayHello: ƒ sayHello
            ()__proto__: Object

```js
Person.prototype.newFeature = 123
123
tom.newFeature
123
```