import { findBooks } from "./books"

describe('Books service', () => {

  test('should find some books', async () => {
    const result = await findBooks({author:''})
    expect(result).toEqual(['Book A'])
  })

})

export { }

